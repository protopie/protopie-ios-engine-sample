//  Copyright © 2017 Studio XID, Inc. All rights reserved.

import UIKit
import ProtoPieEngine

fileprivate let PROTOPIE_LICENSE = "KyuSGY6KttPT4h2U8rrvOo5GGZMGBMpDa02OAsZjRBYta5Bbo+iTqj2dz39MezzQ5KN3dghNIDEzhmTVowOMoxDURXel7xVW3QY6cMrHfjdI+jmpF+oXTy2FQX7DFILj2I8VtJ04zFo53g6a6HCrNGRrW2NW6gzsoP/Zfll6p3t/CzKk8f0U9nZY"

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    
    private var urlOpenListener: UrlOpenListener?
    
    // A URL requested before listener is registered
    private var urlToHandle: URL?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        do {
            try PPLocalPieManager.shared.applyLicense(licenseStr: PROTOPIE_LICENSE)
            return true
        } catch {
            print("Failed to validate ProtoPie SDK license: \(error)")
            fatalError("Failed to validate ProtoPie SDK license: \(error)")
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

    // Called for custom URLs
    // Notify the listener if exists, or just keep it
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        if let listener = urlOpenListener {
            listener.onOpenUrl(url)
        } else {
            urlToHandle = url
        }
        return true
    }
    
    func setUrlOpenListener(_ listener: UrlOpenListener) {
        self.urlOpenListener = listener
        if let urlToHandle = urlToHandle {
            listener.onOpenUrl(urlToHandle)
            self.urlToHandle = nil
        }
    }
}

protocol UrlOpenListener: class {
    func onOpenUrl(_ url: URL)
}
