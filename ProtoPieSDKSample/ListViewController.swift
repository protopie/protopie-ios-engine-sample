//  Copyright © 2017 Studio XID, Inc. All rights reserved.

import UIKit
import ProtoPieEngine

class ListViewController: UIViewController, UITableViewDelegate, UrlOpenListener, ModelListener {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var emptyInstructionView: UIView!
    
    private var piesTableViewDataSource: UITableViewDataSource?

    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
            appDelegate.setUrlOpenListener(self)
        }
        
        Model.shared.addListener(self)
        
        piesTableViewDataSource = PiesTableViewDataSource(localPieManager: PPLocalPieManager.shared)
        tableView.dataSource = piesTableViewDataSource!
        
        tableView.delegate = self
        
        updateEmptyInstructonView()
    }

    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let playViewController = segue.destination as? PlayViewController {
            if let option = sender as? PlayPieOption {
                playViewController.playPieOption = option
            }
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func unwindToList(segue: UIStoryboardSegue) {
    }
    
    // MARK: - UIViewTableDataSource
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        performSegue(withIdentifier: "PLAY_PIE", sender: PlayPieOption.pieForIndex(index: indexPath.row, hideStatusBar: true))
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let delBtn = UITableViewRowAction(style: UITableViewRowActionStyle.normal, title: "Delete") { (action, path) -> Void in
            do {
                try PPLocalPieManager.shared.deleteFileAtIndex((indexPath as NSIndexPath).row)
            } catch {
                let alert = UIAlertController(title: NSLocalizedString("Label.Error", comment: ""), message: NSLocalizedString("Error.DeleteFailure", comment: ""), preferredStyle: .alert)
                let action = UIAlertAction(title: NSLocalizedString("Action.OK", comment: ""), style: .default) { _ in
                }
                alert.addAction(action)
                self.present(alert, animated: true){}
            }
            tableView.reloadData()
            self.updateEmptyInstructonView()
        }
        delBtn.backgroundColor = UIColor(red: 0.243, green: 0.243, blue: 0.243, alpha: 1)
        return [delBtn]
    }
    
    // MARK: - UrlOpenListener
    
    func onOpenUrl(_ url: URL) {
        do {
            let playPieOption = try PlayPieOption.fromUrl(url: url)
            performSegue(withIdentifier: "PLAY_PIE", sender: playPieOption)
        } catch {
            print("Invalid URL: \(error)")
        }
    }
    
    // MARK: - ModelListener
    
    func onItemAdded(lpd: PPLocalPieDescriptor) {
        tableView.reloadData()
        updateEmptyInstructonView()
    }
    
    // MARK: - Private
    
    private func updateEmptyInstructonView() {
        tableView.isHidden = PPLocalPieManager.shared.allPies().count == 0
        emptyInstructionView.isHidden = PPLocalPieManager.shared.allPies().count > 0
    }
    
    private func showLoading() {
        activityIndicatorView.startAnimating()
    }
    
    private func hideLoading() {
        activityIndicatorView.stopAnimating()
    }
}

