import Foundation
import ProtoPieEngine

class Model {
    static let shared: Model = Model()
    
    private let queue = OperationQueue()
    private var listeners = [ModelListener]()
    
    func addListener(_ listener: ModelListener) {
        listeners.append(listener)
    }
    
    func importPie(url: URL, onSuccess: @escaping (PPLocalPieDescriptor) -> Void, onError: @escaping (Error) -> Void) {
        queue.addOperation {
            do {
                let lpd = try PPLocalPieManager.shared.add(url: url)
                
                // Remove the file in Inbox after imported
                let docDirPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                let inboxDirPath = docDirPath + "/Inbox"
                try FileManager.default.removeItem(atPath: inboxDirPath + "/" + url.lastPathComponent)
                
                OperationQueue.main.addOperation {
                    for listener in self.listeners {
                        listener.onItemAdded(lpd: lpd)
                    }
                    
                    onSuccess(lpd)
                }
            } catch {
                onError(error)
            }
        }
    }
}

protocol ModelListener {
    func onItemAdded(lpd: PPLocalPieDescriptor)
}
