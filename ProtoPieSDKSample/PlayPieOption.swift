import Foundation

fileprivate let CUSTOM_URL_SCHEME = "protopiesdksample"
fileprivate let HIDE_STATUS_BAR_PARAM = "hideStatusBar=false"

enum UrlParseError: Error, CustomStringConvertible {
    case numberFormat
    case invalidCustomUrlPath
    case unhandledScheme
    
    var description: String {
        switch self {
        case .numberFormat:
            return "Invalid number"
        case .invalidCustomUrlPath:
            return "Invalid custom URL path"
        case .unhandledScheme:
            return "Unknown URL scheme"
        }
    }
}

enum PlayPieOption {
    case pieForIndex(index: Int, hideStatusBar: Bool)
    case pieForName(name: String, hideStatusBar: Bool)
    case importUrl(url: URL)
    
    func hideStatusBar() -> Bool {
        switch self {
        case .pieForIndex(_, let hideStatusBar):
            return hideStatusBar
        case .pieForName(_, let hideStatusBar):
            return hideStatusBar
        case .importUrl:
            return false
        }
    }
    
    static func fromUrl(url: URL) throws -> PlayPieOption {
        if url.scheme == CUSTOM_URL_SCHEME {
            return try parseCustomUrl(url)
        } else if url.scheme == "file" {
            return PlayPieOption.importUrl(url: url)
        } else {
            throw UrlParseError.unhandledScheme
        }
    }
    
    private static func parseCustomUrl(_ url: URL) throws -> PlayPieOption {
        let components = url.path.components(separatedBy: "/")
        if components.count > 2 {
            let hideStatusBar = (url.query != nil && url.query!.contains("hideStatusBar=false")) ? false : true
            
            if components[1] == "index" {
                if let index = Int(components[2]) {
                    return PlayPieOption.pieForIndex(index: index, hideStatusBar: hideStatusBar)
                } else {
                    throw UrlParseError.numberFormat
                }
            } else if components[1] == "name" {
                return PlayPieOption.pieForName(name: components[2], hideStatusBar: hideStatusBar)
            } else {
                throw UrlParseError.invalidCustomUrlPath
            }
        } else {
            throw UrlParseError.invalidCustomUrlPath
        }
    }
}
