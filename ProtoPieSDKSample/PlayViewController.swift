//  Copyright © 2017 Studio XID, Inc. All rights reserved.

import UIKit
import ProtoPieEngine

class PlayViewController: UIViewController, PPSceneViewDelegate {
    var playPieOption: PlayPieOption?
    
    private let queue = OperationQueue()
    
    @IBOutlet weak var sceneView: PPSceneView!
    @IBOutlet weak var loadingIndicatorView: UIActivityIndicatorView!
    
    // MARK: - UIViewController
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sceneView.delegate = self
        addExitGesture()
        
        if let option = playPieOption {
            run(option)
        } else {
            exitWithError(message: "No PlayPieOption") // Never happens
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override var prefersStatusBarHidden: Bool {
        return playPieOption?.hideStatusBar() ?? false
    }
    
    // MARK: - PPSceneViewDelegate
    
    func sceneCaptured(_ capturedImage: UIImage) {
        // Nothing to do
    }
    
    func sendMessageToStudio(_ msg: PPMsg) {
        let json = msg.toJson()
        if json["messageId"] == "#EXIT" {
            performSegue(withIdentifier: "EXIT_PIE", sender: self)
        }
    }
    
    // MARK: - Private
    
    private func addExitGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(onExitGesture))
        tap.numberOfTapsRequired = 2
        tap.numberOfTouchesRequired = 2
        self.view.addGestureRecognizer(tap)
    }
    
    private func run(_ option: PlayPieOption) {
        switch option {
        case .pieForIndex(let index, _):
            if index >= 0 && index < PPLocalPieManager.shared.allPies().count {
                startPie(lpd: PPLocalPieManager.shared.allPies()[index])
            } else {
                exitWithError(message: "No pie with index: \(index)")
            }
        case .pieForName(let name, _):
            if let lpd = findPieBy(name: name) {
                startPie(lpd: lpd)
            } else {
                exitWithError(message: "No pie with name: \(name)")
            }
        case .importUrl(let url):
            importPie(url: url, callback: { lpd in
                startPie(lpd: lpd)
            })
        }
    }
    
    private func importPie(url: URL, callback: (PPLocalPieDescriptor) -> Void) {
        showLoading()
        
        Model.shared.importPie(url: url, onSuccess: { lpd in
            self.startPie(lpd: lpd)
        }, onError: { error in
            print("Failed to import a pie file: \(error)")
            self.hideLoading()
            self.exitWithError(message: "Failed to import a pie pile: \(error)")
        })
    }
    
    private func startPie(lpd: PPLocalPieDescriptor) {
        showLoading()
        
        queue.addOperation {
            do {
                let pie = try PPLocalPieManager.shared.load(id: lpd.id)
                
                OperationQueue.main.addOperation {
                    pie.model.applyScaleFactor(width: Float(self.sceneView.frame.size.width), height: Float(self.sceneView.frame.size.height))
                    self.sceneView.startPie(pie: pie)
                    self.hideLoading()
                }
                
            } catch {
                OperationQueue.main.addOperation {
                    print("Failed to load a pie: \(error)")
                    self.hideLoading()
                    self.exitWithError(message: "Failed to load a pie: \(error)")
                }
            }
        }
    }
    
    @objc
    private func onExitGesture() {
        print("Exit gesture detected")
        performSegue(withIdentifier: "EXIT_PIE", sender: self)
    }
    
    private func exitWithError(message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: { _ in
            self.performSegue(withIdentifier: "EXIT_PIE", sender: self)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func showLoading() {
        loadingIndicatorView.isHidden = false
    }
    
    private func hideLoading() {
        loadingIndicatorView.isHidden = true
    }
    
    private func findPieBy(name: String) -> PPLocalPieDescriptor? {
        for lpd in PPLocalPieManager.shared.allPies() {
            if lpd.name == name || lpd.name == "\(name).pie" {
                return lpd
            }
        }
        return nil
    }
}
