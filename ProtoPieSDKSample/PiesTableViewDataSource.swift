//  Copyright © 2017 Studio XID, Inc. All rights reserved.

import UIKit
import ProtoPieEngine

class PiesTableViewDataSource: NSObject, UITableViewDataSource {
    private let localPieManager: PPLocalPieManager
    
    init(localPieManager: PPLocalPieManager) {
        self.localPieManager = localPieManager
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localPieManager.allPies().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PieCell", for: indexPath)
        let pie = localPieManager.allPies()[indexPath.row]
        cell.textLabel?.text = "[\(indexPath.row)] \(pie.name)"
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}
