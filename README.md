# A Sample App for ProtoPie iOS Engine SDK

This is a sample app that uses ProtoPie iOS Engine SDK.

You can add pie files into this app and run them with the app UI or custom URLs.

# Installation

Install the cocoapod modules.
```
pod repo update
pod install
```

Open `ProtoPieSDKSample.xcworkspace` with Xcode.

# Usages

## Adding and Deleting Pie Files

Download pie files with apps like Safari and open the file with this app.
Then the file is added to the list of this app and you can run it from the list.

To delete items in the list, swipe items to left and tap on the Delete button.

## Run Items with Custom URL

It supports the following URLs:

* protopiesdksample:///index/{index}
* protopiesdksample:///name/{name}

For example, protopiesdksample:///index/0 will open the first item in the list.

To hide the status bar while running pies append `?hideStatusBar=false` to the URL like this:

* protopiesdksample:///index/0?hideStatusBar=false
